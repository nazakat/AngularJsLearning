import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppNavigation } from "./navigation/navigation";
import { AppProductList } from "app/products/product";
import { AppInfobox } from "app/infobox/infobox";
import { AppReactiveForm } from "app/reactiveForm/reactiveForm";
import { errorCont } from "app/reactiveForm/reactiveFormOne";



@NgModule({
  declarations: [
    AppComponent,
    AppNavigation,
    AppProductList,
    AppInfobox,
    AppReactiveForm,
    errorCont
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
