import { Component } from "@angular/core";
import { AppProduct } from "./product-service";
import { Pipe, PipeTransform } from "@angular/core"

@Component({
    selector: 'app-product',
    templateUrl: './product.html',
    providers: [AppProduct]
})
export class AppProductList {
    products: product[];

    constructor(private _product : AppProduct){}

    ngOnInit() {
        this.addData()
    }
    addData(){
        this._product.getData().subscribe(products =>{
            this.products = products
        });
    }
}
interface product {
    id: number,
    title: string
}
