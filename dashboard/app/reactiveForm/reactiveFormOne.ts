import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
@Component({
    selector: 'app-error',
    template: `<span *ngIf="errors.exist">{[msgs.exist?.msgs.exit: 'field exist']}</span>`
})

export class errorCont {
    @Input() errors:any;
    @Input() msgs = '';
}