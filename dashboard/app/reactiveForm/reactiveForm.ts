import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";

@Component({
    selector: 'app-reactiveForm',
    templateUrl: './reactiveForm.html'
})

export class AppReactiveForm implements OnInit {
    rForm: FormGroup;
    fromError = { f_fname: '', f_lname: '', f_email: '', f_mobile: '', f_gender: '' }

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.rForm = this.fb.group({
            f_fname: ['', [Validators.required, Validators.minLength(3)]],
            f_lname: ['', [Validators.required]],
            f_email: ['', [Validators.required]],
            f_mobile: ['', [Validators.required]],
            f_gender: ['', [Validators.required]],
        });
        this.rForm.valueChanges.subscribe(data => this.validationForm());
    }

    validationMessage = {
        f_fname: {
            required: 'Enter your first name',
            minlength: 'Enter your less then 3 charectors',
        },
        f_lname: {
            required: 'Enter your last name',
        },
        f_email: {
            required: 'Enter your email',
        },
        f_mobile: {
            required: 'Enter your mobile',
        },
        f_gender: {
            required: 'Enter your gender',
        }
    }

    validationForm() {
        for (let field in this.fromError) {
            this.fromError[field] = '';
            let input = this.rForm.get(field);
            if (input.invalid && input.dirty) {
                for (let error in input.errors) {
                    this.fromError[field] = this.validationMessage[field][error];
                }
            }
        }

    }

    submitForm() {
        console.log(this.rForm.value);
    }

}